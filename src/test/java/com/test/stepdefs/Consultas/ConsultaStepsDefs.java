package com.test.stepdefs.Consultas;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import com.test.apidemo.app.screens.AbstractScreen;
import com.test.steps.Consultas.ConsultaSteps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ConsultaStepsDefs extends AbstractScreen{

private Scenario scenario;
	
	@Autowired
    public ConsultaStepsDefs(AppiumDriver<? extends MobileElement> driver) {
    	
                   super(driver);
                   // TODO Auto-generated constructor stub
    }
	
	@Autowired
	private ConsultaSteps objConsultasSetps;
	
	@Before
    public void before(Scenario scenario){
        this.scenario=scenario;
	}
	
	@Given("^El aplicativo cargo en pantalla$")
    public void validarSesion(){
        try {
            System.out.println("Session ID: " + driver.getSessionId());
            capturaPantalla(scenario);
            objConsultasSetps.seleccionarClara();
            capturaPantalla(scenario);
        } catch (Exception e) {
            System.out.println("MSJ ERROR CAUSE: " + e.getCause());
            System.out.println("MSJ ERROR: " + e.getMessage());
        }

    }
    @When("^Iniciar una conversacion (.*?)$")
    public void iniciarConversacion(String sTexto) {
    	objConsultasSetps.escribirConversacion(sTexto); 
    	capturaPantalla(scenario);
    	}

    @And("^Seleccionar la opcion (.*?)$")
    public void selecionarOpcion(String sTexto) {
    	objConsultasSetps.selecionarMenu(sTexto);
    	capturaPantalla(scenario);
    }
    @Then("^Validar las cuentas del cliente$")
    public void validarCuenta() {
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	capturaPantalla(scenario);
        //Assert.assertFalse(objConsultasSetps.validarCuentaCliente());
    	System.out.print("OK");
    }


}
