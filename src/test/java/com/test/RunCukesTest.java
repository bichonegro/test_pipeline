package com.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import framework.appium.support.server.AppiumServer;
import framework.server.ServerArguments;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "json:target/cucumber.json", features = {
//@reemplazar_inicio
"src/test/resources/features/Consulta.feature"},
//@reemplazar_fin
        glue = "com.test.stepdefs")
public class RunCukesTest {

    private static AppiumServer _appiumServer;

    @BeforeClass
    public static void startAppium() {
    	
    	try{
    		
    	ServerArguments serverArguments = new ServerArguments();
        serverArguments.setArgument("--address", "127.0.0.1");
        serverArguments.setArgument("--no-reset", true);
        serverArguments.setArgument("--local-timezone", false);

        _appiumServer = new AppiumServer(new File("C:/Appium/node.exe"),
                new File("C:/Appium/node_modules/appium/bin/appium.js"), serverArguments);
      
        
        _appiumServer.startServer();   
        _appiumServer.startServer(100);
    	}catch(Exception e){
    		System.out.println("Revisa nuevamente clase RunCukes");
    	}
        
    }

    @AfterClass
    public static void stopAppium() {
    	    		 
    	_appiumServer.stopServer();
    }
}


