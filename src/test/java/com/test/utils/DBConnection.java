package com.test.utils;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.ResultSet;
import java.sql.Statement;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class DBConnection {
	
	public Connection crearConexion() throws IOException  {
    	InputStream inputStream;
    	String dbuser  = "";
    	String database  = "";
    	String dbpassword = "";
    	
		 Connection con=null;
		 try {
			 			 
			 	Properties prop = new Properties();
				String propFileName = "config_test_app.properties";
	 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
	 
				dbuser = prop.getProperty("dbuser");
				database = prop.getProperty("database");
				dbpassword  = prop.getProperty("dbpassword");
								
				
			Class.forName("COM.ibm.db2os390.sqlj.jdbc.DB2SQLJDriver");
		
//	con = DriverManager.getConnection("jdbc:db2://10.80.198.62:51000/DBMBBK" ,"apbmcer","e1Foyw7k");
			con = DriverManager.getConnection(database,dbuser,dbpassword);
			
			
		 } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 } 
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
		 
		return con;
	}
	
	public String ObtenerCodigo(String tarjeta) throws IOException  
	{
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		String codigo="";
		
		String query = "SELECT VALIDATIONCODE, CREATIONTIME FROM DBMBBKALT.SESSION where CREDIMAS = '" + tarjeta + "' AND VALIDATIONCODE IS NOT NULL AND VALIDATIONCODE <> '' order by 2 desc FETCH FIRST 1 ROWS ONLY";
		System.out.println("Consulta realizada = "+query);
		
		try {
			con = crearConexion();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			
			while (rs.next()) {
			  codigo = rs.getString("VALIDATIONCODE");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("CODIGO: "+codigo);
		return codigo;
	}
	
	public String ObtenerCodigoPagoCompartido(String correo) throws IOException  
	{
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		String codigo="";
		
		String query = "SELECT DATELASTREMEMBER, VALIDATIONCODE FROM DBMBBKALT.PAYMENT where DESTINATIONUSER = '" + correo + "' AND VALIDATIONCODE IS NOT NULL AND VALIDATIONCODE <> '' order by 1 desc FETCH FIRST 1 ROWS ONLY";	
		
		System.out.println("Consulta realizada = "+query);
		try {
			con = crearConexion();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
			  codigo = rs.getString("VALIDATIONCODE");
			  System.out.println("Codigo de validacion = "+codigo);		
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return codigo;
	}

	
	
}

