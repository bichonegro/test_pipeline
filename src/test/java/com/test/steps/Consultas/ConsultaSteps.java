package com.test.steps.Consultas;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import com.test.apidemo.app.screens.AppScreen;
import com.test.steps.AbstractSteps;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import junit.framework.Assert;

@Component
@Scope("cucumber-glue")
public class ConsultaSteps extends AbstractSteps{
	
	    public void seleccionarClara() {
	    	appScreen.seleccionarClara();
	    }

	    public void escribirConversacion(String texto) {
	    	appScreen.escribirConversacion(texto);
	    }

	    public void selecionarMenu(String texto) {
	    	appScreen.selecionarMenu(texto);
	    }

	    public boolean validarCuentaCliente() {
	        try {
	            return true;
	        } catch (Exception e) {
	            return false;
	        }

	    }
}
