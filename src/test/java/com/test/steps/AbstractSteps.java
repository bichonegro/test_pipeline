package com.test.steps;

import org.springframework.beans.factory.annotation.Autowired;

import com.test.apidemo.app.screens.AppScreen;
import com.test.utils.AppUtils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AbstractSteps {
	

	@Autowired
    public AppUtils utils;

    @Autowired
    public AppScreen appScreen;
}
