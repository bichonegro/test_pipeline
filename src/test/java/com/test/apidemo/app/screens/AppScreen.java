package com.test.apidemo.app.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.remote.MobileCapabilityType;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.filters.LineContains.Contains;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.KeyDownAction;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

//import com.ibm.db2.jcc.am.*;
import com.test.utils.DBConnection;

import cucumber.api.Scenario;

@Component
@Scope("cucumber-glue")
public class AppScreen extends AbstractScreen {

	private Scenario scenario;

	private String TipoCambio;
	private String Contravalor;
	
    @AndroidFindBy(accessibility = "Activity")
    private AndroidElement appActivityElement;

    @AndroidFindBy(accessibility = "Notification")
    private WebElement appNotificationElement;
    
    @AndroidFindBy(id = "com.whatsapp:id/conversations_row_contact_name")
    MobileElement iconoClara;    
    @AndroidFindBy(id = "com.whatsapp:id/entry")
    MobileElement ingresoTexto;
    
    @AndroidFindBy(id = "com.whatsapp:id/send")
    MobileElement sendmensaje;

    @Autowired
    public AppScreen(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    
    public void seleccionarClara()  {
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	iconoClara.click();
    }

    public void escribirConversacion(String texto) {
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	ingresoTexto.sendKeys(texto);
    	sendmensaje.click();
    }

    public void selecionarMenu(String texto) {
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	ingresoTexto.sendKeys(texto);
    	sendmensaje.click();
    }

    public boolean validarCuentaCliente() {

        try {
            return true;
        } catch (Exception e) {
            return false;
        }

    }
    
	public String getTipoCambio() {
		return TipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		TipoCambio = tipoCambio;
	}

	public String getContravalor() {
		return Contravalor;
	}

	public void setContravalor(String contravalor) {
		Contravalor = contravalor;
	}
	
    ///////////////////////
    //METODOS GENERALES
    //////////////////////
    

    public void clickOnButton(String id){
       driver.findElement(By.id(id)).click();
    }
    
    public void clickOnButtonByXPath(String tipo,String texto){
    	driver.findElement(By.xpath("//android.widget."+tipo +"[contains(@text,'" + texto +"')]"));	
    }
    
    public void clickOnButtonByXPath2(String control ,String texto)
    {
    driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto + "']")).click();

    }

    public boolean IsDisplayedByXPathText(String texto){
       return driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + texto +"')]")).isDisplayed();
       
    }
    
    public void ClickByXPathText(String texto){
       driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + texto +"')]")).click();
    }
    
    
    public void Scrol(String texto){
    	driver.scrollTo(texto).click();
    	
    }

	public void OcultarTeclado()
	{
		try
		{
			driver.hideKeyboard();
		}
		catch (Exception ex)
		{	
		}		
	}
	
	public void SeleccionarBoton(String texto){
		
//    	driver.findElementByXPath("//android.widget.Button[@text='" + texto +"']").click();
    	loadingpathclick(100, "Button", texto);
	}
	
	public String CapabilityBasePKG(){
		return driver.getCapabilities().getCapability(MobileCapabilityType.VERSION).toString();
	}
	
	public String CapabilityDeviceName(){
		return driver.getCapabilities().getCapability(MobileCapabilityType.DEVICE_NAME).toString();
	}
   
    
    public void metodoBackEspc(){
    	
    	driver.navigate().back();

    }
    
    public void EnviarCorreoOpcional(String valor)
    {
    	driver.findElement(By.xpath("//android.widget.EditText[@text='E-mail destinatario (opcional)']")).click();
    	driver.findElement(By.xpath("//android.widget.EditText[@text='E-mail destinatario (opcional)']")).sendKeys(valor);  
    	driver.navigate().back();
    	driver.navigate().back();
    }
    
    public void ValidarTipoTarjeta(String tarjeta) throws InterruptedException
    {
    	
 	   if(tarjeta.substring(0, 2).equals("12"))
 		{
 			 MobileElement cambiarTarjeta= driver.findElement(By.id("llFrontForm"));
 			 int x,y;
 			 x=  cambiarTarjeta.getSize().getWidth();
 			 y=	cambiarTarjeta.getLocation().getY();
 			
 			if(CapabilityDeviceName().equals("ce0616063214013b03") || CapabilityDeviceName().equals("ce05160522e5753a04") ){
 				 driver.swipe(x, y, x-1250, y, 3500);
 	 			 driver.swipe(x, y, x-1250, y, 3500);
 	    	}
 			else
 			{
 			 driver.swipe(x, y, x-600, y, 3000);
 			 driver.swipe(x, y, x-600, y, 3000);
 			}
 		 }
    }
    
    
    public void loadingpathclick(int tries, String control, String texto){
    	for(int i=0; i<=tries;i++){
	    	try{
	    		By byelemt = By.id(CapabilityBasePKG()+":id/ivLoading");	    		
	    		if(driver.findElement(byelemt).isDisplayed()){
	    			//System.out.println("   objeto 'id/ivLoading' encontrado " + i); 			
	    			continue;
	    		}
	    	}
	    	catch(org.openqa.selenium.NoSuchElementException ex){	
	    		try{
//	    			System.out.println("   Intentando encontrar objeto 'id/tvTitle' " + i);
//		    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).isDisplayed()){
//		    			System.out.println("   objeto 'id/tvTitle' encontrado");
		    			try{
		    				driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto  + "']")).click();
		    				System.out.println("   Se encontró " + control.toUpperCase() + " '" + texto + "' " + i);		    				
		    			}catch(org.openqa.selenium.NoSuchElementException ex4){	
		    				System.out.println("   No se encontró " + control.toUpperCase() + " '" + texto + "' " + i);
		    			}
//		    		}
		    		break;	    			
	    		}catch(org.openqa.selenium.NoSuchElementException ex3){
	    			try{
		    			System.out.println("   Intentando encontrar botón '" + texto + "'");
		    			driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto  + "']")).click(); //botón "Más tarde"	
		    			System.out.println("   Click en botón ' " + texto + "'");
			    		break;
		    		}
		    		catch(org.openqa.selenium.NoSuchElementException e){	    			
			    		try{
			    			System.out.println("   Intentando encontrar objeto 'id/tvTitle'");
				    		driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle"));
				    		System.out.println("   Carga de cuentas finalizada");
				    		break;		    			
			    		}catch(org.openqa.selenium.NoSuchElementException ex2){		    
			    			System.out.println("   ...");
			    	    	continue;				    		
			    		}
		    		}
	    		}
	    	}
	    }
    	System.out.println("   Sale bucle");
    }
    
    public void loadingpathclick2(int timeOut, String control, String texto){
    	try{
	    	int time = 0;    	
	    	By byelemt = By.id(CapabilityBasePKG()+":id/ivLoading");
	    	do {
	    		time ++;
	    		//System.out.println("   ivLoading " + time);
	    		if(time==timeOut){
	    			System.out.println("   TIME OUT, se esperaba " + control.toUpperCase() + " '" + texto + "'");	
	    			Assert.fail();
	    		}
	    	} while (driver.findElement(byelemt).isDisplayed());    
	    	
	    	try{
	    		driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto  + "']")).click();
	    		System.out.println("   Se encontró " + control.toUpperCase() + " '" + texto + "'1");	
	    	}catch(Exception e2){
	    		System.out.println("   No se encontró " + control.toUpperCase() + " '" + texto + "'1");	
	    	}
    	}catch(Exception e){
    		try{
	    		driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto  + "']")).click();
	    		System.out.println("   Se encontró " + control.toUpperCase() + " '" + texto + "'2");	
	    	}catch(Exception e3){
	    		System.out.println("   No se encontró " + control.toUpperCase() + " '" + texto + "'2");	
	    	}
	    }
    }
    
    public void CargaEnProceso(int timeOut){
	    try{
	    	int time = 0;    	
			By byelemt = By.id(CapabilityBasePKG()+":id/ivLoading");
			do {
				time ++;
				if(time==timeOut){
					System.out.println("   TIME OUT");		
				}
			} while (driver.findElement(byelemt).isDisplayed());
	    }catch(Exception e){
	    	System.out.println("   listo");
	    }
    }
    
    
    public void click_boton_mas_tarde(){
    	try{
	    	System.out.println("   Intentando encontrar botón 'Más tarde'");
	    	driver.findElement(By.xpath("//android.widget.button[@text='Más tarde']")).click();
			System.out.println("   Click en botón 'Más tarde'");
    	}
    	catch(org.openqa.selenium.NoSuchElementException e){
    		
    	}
    }

    
    public void loadingclickwithScrollByPath(int tries, String control, String texto)
    {
    	for(int i=0; i<=tries;i++)
    	{
	    	try
	    	{ 
	    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/ivLoading")).isDisplayed())
	    		{
	    			System.out.println("continue 2.0");
	    			continue;
	    		}
	    		
	    	}
	    	catch(org.openqa.selenium.NoSuchElementException ex)
	    	{
	    		try{
	    			driver.findElement(By.xpath("//android.widget." + control + "[@text='" + texto  + "']")).click();
	    			
	    			break;
	    		}
	    		catch(org.openqa.selenium.NoSuchElementException e)
	    		{
	    			driver.scrollTo(texto);
	    			continue;
	    		}
	    	}
	    	
	    }
    }
    
    
    public void loadingclickwithScrollById(int tries, String id, String texto)
    {
    	for(int i=0; i<=tries;i++)
    	{
	    	try
	    	{ 
	    		
	    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/ivLoading")).isDisplayed())
	    		{
	    			System.out.println("continue 2.0");
	    			continue;
	    		}
	    		
	    	}
	    	catch(org.openqa.selenium.NoSuchElementException ex)
	    	{
	    		try{
		    		driver.findElement(By.id(id)).click();
		    		System.out.println("dio click");
		    		break;
	    		}
	    		catch(org.openqa.selenium.NoSuchElementException e)
	    		{
	    			driver.scrollTo(texto);
	    			continue;
	    		}
	    	}
	    	
	    }
    }
    
    
    public void loadingclick(int tries, String id) throws Exception{
    	boolean cargando=false;    	
    	for(int i=0; i<=tries;i++){
	    	try{ 	    		
	    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/ivLoading")).isDisplayed()){
	    			if(cargando == false){
	    				System.out.println("   Obteniendo información de usuario");
		    			cargando = true;
	    			}
	    			continue;
	    		}	    		
	    	}catch(org.openqa.selenium.NoSuchElementException ex){
	    		try{
		    		driver.findElement(By.id(id)).click();
		    		System.out.println("   Elige donde recibir el código de validación");
		    		break;
	    		}
	    		catch(org.openqa.selenium.NoSuchElementException e){
	    			try	{
	    				driver.findElement(By.xpath("//android.widget.Button[@text='Reintentar']"));
	    				continue;
	    			}
	    			catch(org.openqa.selenium.NoSuchElementException ex2){
	    				
	    				if (driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).isDisplayed())
	    				{
	    					throw new Exception("Error al cargar la pantalla");
	    				}
	    				
	    				System.out.println("   continue 1.0");
	    				continue;
	    			}
	    		}
	    	}
	    	
	    }
    }
    
    
    
	public void loading(int tries, String id) throws Exception {		
		for(int i=0; i<=tries;i++){
	    	try	{ 
	    		System.out.println("   Intentando encontrar objeto 'id/ivLoading'");
	    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/ivLoading")).isDisplayed()){
	    			System.out.println("   continue 2 load: Encontrado");
	    			
	    			MobileElement elem;
	    			elem = driver.findElement(By.xpath("//android.widget.Button[@text='Reintentar']"));
	    			if (elem != null)
	    			{
	    				throw new Exception("Error al cargar la pantalla");
	    			}
	    			
	    			continue;
	    		}	    		
	    	}
	    	catch(org.openqa.selenium.NoSuchElementException ex){	
	    		try	{
    				System.out.println("   Intentando encontrar objeto " + id);
    				if(driver.findElement(By.id(id)).isDisplayed());{
			    		System.out.println("   Esta presente: Encontrado");
			    		break;
	    			}
    			}
    			catch(Exception ex1){
	    			System.out.println("   continue 3 load");
	    			
	    			
	    			
	    			
	    			
	    			if(driver.findElementById(CapabilityBasePKG()+":id/bButtonPermise").isDisplayed()){
	   				 	capturaPantalla(scenario);
	   				 	driver.findElementById(CapabilityBasePKG()+":id/bButtonPermise").click();
	   				 	capturaPantalla(scenario);
	   				 	//objAppScreen.driver.findElement(By.name("Permitir")).click();
	   				 	driver.findElement(By.name("Rechazar")).click();
	   				 	Thread.sleep(1000);
	   				 	capturaPantalla(scenario);
	   				 	//objAppScreen.driver.findElement(By.name("Permitir")).click();
	   				 	driver.findElement(By.name("Rechazar")).click();
	   			 	}
	    			
	    			continue;
    			}
	    	}
	    }
	} 
	
	public void EnviarTexto(String claveInternet){
	   String digito = "";	   
	   for(int i=0;i<6;i++){
		   if(claveInternet.length()>1){
			   digito = claveInternet.substring(1,2);
		   }
		   else{
			   digito = claveInternet;			   
		   }
		   claveInternet = claveInternet.substring(1);		   
		   BuscarDigitoTeclado(digito);
	   }
	}
	
    
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    public void clickOnActivityLable() {
        appActivityElement.click();
        driver.scrollTo("Secure Surfaces");
    }

	public boolean VerificarControlVisible(String id)
	{	
		if(driver.findElement(By.id(id)).isDisplayed()){
			return true;
		}		
		return false;
	} 
	
    public void clickOnNotificationLable() {
        appNotificationElement.click();
    }
    
    public void SendKeys(String id, String valor)
    {
    	
    	switch( driver.findElement(By.id(id)).getText().toString())
    	{
	    	case "":
	    		driver.findElement(By.id(id)).sendKeys(valor);    	
	    	break;
	    	case "Número de documento":
	    		driver.findElement(By.id(id)).sendKeys(valor);   
	    	break;
	    	case "Código validación":
	    		driver.findElement(By.id(id)).sendKeys(valor);    	
	    	break;	    	
    	}    	
    }
        
    public void BuscarDigitoTeclado(String numero){
    	int i;
    	for(i=0;i<12;i++){    		
    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/b" + Integer.toString(i))).getText().toString().compareTo(numero) == 0){
    			clickOnButton(CapabilityBasePKG()+":id/b" + i);
    			break;
    		}
    	}    	
    }
    

    
    public void BuscarDigitoTecladoMontoTransferencia(String numero)
    {
    	int i;
    	for(i=0;i<12;i++)
    	{
    		
    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/etInput" + Integer.toString(i))).getText().toString().compareTo(numero) == 0) 
    		{
    			clickOnButton(CapabilityBasePKG()+":id/etInput" + i);
    			break;
    		}
    	}
    	
    }
 

    public void Enviarcodigo(String tarjeta) throws Exception 
    {
    	List<? extends MobileElement> elementList = null;    	
    	try	{    	
    		Thread.sleep(2000);
    	
    		String codigovalidacion = "";
        
    		DBConnection conexion = new DBConnection();
    		codigovalidacion = conexion.ObtenerCodigo(tarjeta);
    		
    		elementList = driver.findElements(MobileBy.id(CapabilityBasePKG()+":id/etInput"));
    		System.out.println("   elementos: " + elementList);
        	
    		boolean cargando = false;
        	for(MobileElement elem : elementList)
        	{   
        		if (elem.getText().contains("digo validaci")) elem.sendKeys(codigovalidacion); //elem.sendKeys("12345"); 
        		//ingresa código de validación
        		if(cargando == false){
    				System.out.println("   Ingresa código de validación");
        			cargando = true;
    			}
        	}
        	
        	OcultarTeclado();
 
    	}
    	catch (Exception ex)
    	{
    		throw ex;
    	}
    }
    
    
    public void EnviarNumeroCuenta(String cuenta)
    {
    	
    	try {
    		
    		if(driver.findElement(By.id(CapabilityBasePKG()+":id/layDestiny")).isDisplayed()){
    		driver.findElement(By.id(CapabilityBasePKG()+":id/layDestiny")).click();
    		driver.findElement(By.id(CapabilityBasePKG()+":id/etInput")).sendKeys(cuenta);
    		
    	driver.navigate().back();
    	driver.navigate().back();
    		} else{
    			System.out.println("no entró elemento no encontrado");
    		}
		} catch (Exception e) {
		
		}    	

    }
    
    
    
    public void buttonCloseInmediatas(){
    	driver.findElementByXPath("//*[@class='android.widget.Button' and @text='Cerrar']").click();
    }
    
      
	
    public void Enviaridc(String valor, int tries) throws Exception
    {
    	List<? extends MobileElement> elementList = null;
    	
    	try
    	{
    	
    		WebDriverWait wait = new WebDriverWait(driver, 500);
    	
    		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.bcp.bank.bcp:id/tvInput")));
    		
    		elementList = driver.findElements(MobileBy.id(CapabilityBasePKG()+":id/etInput"));
    	
    		for(MobileElement elem : elementList)
    		{   
    			boolean cargando = false;
    			if (elem.getText().contains("mero de documento")) 
    			{
    				elem.sendKeys(valor);
    			}
    			else if(elem.getText().contains("mero de RUC"))
    			{
    				elem.sendKeys(valor);
    			}
    			///////////////////////////
        		if(cargando == false){
    				System.out.println("   Ingresa nro de documento");
        			cargando = true;
    			}
    		}
    	}
    	catch (Exception ex)
    	{
    		throw ex;
    	}
    }
    
    public void seleccion(String texto){		
		
		MobileElement elemento = driver.findElementByXPath("//android.widget.RelativeLayout/android.widget.ScrollView/android.widget.FrameLayout");
        int posicionX = elemento.getLocation().getX();
        int posicionY = elemento.getLocation().getY();
        
        for(int contador=0;contador<5;contador++){        	
        	try{
        		//if (CapabilityDeviceName().equals("LGH81554bed16e")){//LG G3
        		//	driver.findElementByXPath("//android.widget.RelativeLayout/android.widget.TextView[@text='" + texto + "']").click();
        		//}else{
        			driver.findElement(By.name(texto)).click();
        		//}
				break;
			}
			catch(org.openqa.selenium.NoSuchElementException ex){			
				if (CapabilityDeviceName().equals("ZY2236767P") || CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002") || CapabilityDeviceName().equals("MWS7N17118009373")){
					driver.swipe(posicionX, posicionY, posicionX, posicionY -300, 4000);
				}else{
					 driver.swipe(posicionX, posicionY, posicionX, posicionY -600, 4000);
				}				
			}        
		}        
    }
    
        	
	public void EnviarTipoDeMoneda(String valor) throws InterruptedException{

		String valorActual = "";
		valorActual = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/csSpinner']/android.widget.TextView").getText();
		
		if(!valorActual.equals(valor)){
			MobileElement elemento = driver.findElementById(CapabilityBasePKG()+":id/csSpinner");
		    int posicionX;
		    int posicionY;
		    
		    posicionX = elemento.getLocation().getX();
		    posicionY = elemento.getLocation().getY();
		    TouchAction action = new TouchAction((MobileDriver)driver);
		 
		    if (CapabilityDeviceName().equals("ZY2236767P") ||  CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002") ){
		    	if(valor.toUpperCase().equals("DOLARES")){   
	    	  		elemento.click();
	    	  		action.tap(posicionX +20,posicionY + 180).perform();   
	             }
	    	  	else{
	    	  		elemento.click();
	    	  		action.tap(posicionX +20,posicionY + 80).perform();   
	    	  	}		    	   
		    }
		    else if (CapabilityDeviceName().equals("MWS7N17118009373")){
		    	if(valor.toUpperCase().equals("DOLARES")){   
	    	  		elemento.click();
	    	  		action.tap(posicionX +30,posicionY + 250).perform();   
	             }
	    	  	else{
	    	  		elemento.click();
	    	  		action.tap(posicionX +30,posicionY + 120).perform();   
	    	  	}		    	   
		    }
		    else{
		    	if(valor.toUpperCase().equals("DOLARES")){
		    		elemento.click();
		    		action.tap(posicionX +20,posicionY + 350).perform();   
		        }
		    	else{
		    		elemento.click();
		    	  	action.tap(posicionX +20,posicionY + 150).perform();   
		    	 }
		    }	
			//driver.hideKeyboard();			
			OcultarTeclado();			
			driver.navigate().back();
		}
		else{
			return;
		}
	}
	
    
	public void SeleccionarCuentas(String origen,MobileElement elemento){
		
    	int x=elemento.getLocation().getX();
    	int y=elemento.getLocation().getY();
    	
    	if(elemento != null){
    		for(int a=0; a<5 ; a++){    			
    			try {    				
    				driver.findElement(By.name(origen)).click();    			
    				//driver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'tvNumber') and @text='"+origen+"']")).click();
    				break;
				} catch (org.openqa.selenium.NoSuchElementException ex) {					
					if(CapabilityDeviceName().equals("ZY2236767P") || CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002") || CapabilityDeviceName().equals("MWS7N17118009373")){						
						driver.swipe(x,y,x,y -(300), 3000);
					}else{
						driver.swipe(x,y,x,y -(600), 3000);
					}
				}
    		}
    	}    	
    }

	
	public void EnviarMontoPagoServicio(String valor){		
		try{
			MobileElement elemento = driver.findElementByXPath("//android.widget.LinearLayout[@index='1']/android.widget.EditText[@index='1']");			
			elemento.click();			
		}catch(Exception e2){
			//
		}
		driver.findElementById(CapabilityBasePKG()+":id/etInput").sendKeys(valor);
	}
	
	
	public void EnviarMontoPagoTarjetas(String valor){
		try{
			MobileElement elemento = driver.findElementByXPath("//android.widget.LinearLayout[@index='1']/android.widget.EditText[@index='1']");		
			elemento.click();
		}catch(Exception e2){
			//
		}
		driver.findElementById(CapabilityBasePKG()+":id/etInput").sendKeys(valor);		
/*		
		if(driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).getText().contains("Pago de Tarjetas Otros bancos"))
		{
			//MobileElement elemento = driver.findElementsById(CapabilityBasePKG()+":id/tvTitle").get(1);
			//elemento.click();
		}
		else{//otras financieras
			MobileElement elemento = driver.findElementsById(CapabilityBasePKG()+":id/etInput").get(1);
			elemento.click();
		}
		driver.findElementById(CapabilityBasePKG()+":id/etInput").sendKeys(valor);
*/
	}

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /**PAGO DE TARJETAS DE CREDITO
     * @throws InterruptedException **/
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    
	public void IngresarTipooMonto(String texto) throws InterruptedException{
		
		String [] txt;
		String tipo;
		String monto="0";
		
		txt = texto.split("/");
		tipo=txt[0];
		if(texto.split("/").length > 1)
		monto = txt[1];
		
		MobileElement elemento= driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/layAmount']/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/csSpinner']");
		
		int x=elemento.getLocation().getX();
		int y=elemento.getLocation().getY();
		
		TouchAction touch=new TouchAction(driver);
		
		if (CapabilityDeviceName().equals("ZY2236767P") || CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")) {
			switch (tipo.toUpperCase()) {
			case "MINIMO":
				elemento.click();
	       	  	touch.tap(x +10,y + 180).perform();   
				break;
			case "OTRO":
				driver.findElement(MobileBy.xpath("//android.widget.EditText[@resource-id='"+CapabilityBasePKG()+":id/etInput']")).click();
				driver.findElement(MobileBy.xpath("//android.widget.EditText[@resource-id='"+CapabilityBasePKG()+":id/etInput']")).clear();
				driver.findElement(MobileBy.xpath("//android.widget.EditText[@resource-id='"+CapabilityBasePKG()+":id/etInput']")).sendKeys(monto);
				
				/*elemento.click();
	       	  	touch.tap(x +10,y + 200).perform();   */
				
				
	       	
				break;
				
			default:
				break;
			}
			
		}else{
			switch (tipo.toUpperCase()) {
			case "MINIMO":
				
				elemento.click();
	       	  	touch.tap(x +20,y + 370).perform();  
	       	  
				break;
			case "OTRO":
				
				elemento.click();
	       	  	touch.tap(x +20,y + 470).perform();   
	       	  	driver.findElement(MobileBy.xpath("//android.widget.EditText[@resource-id='"+CapabilityBasePKG()+":id/etInput']")).sendKeys(monto);
				break;
				
			default:
				break;
			}
		}	
		
		if(tipo.toUpperCase().equals("OTRO"))
		{
			driver.navigate().back();
			driver.navigate().back();
		}
	
	}
	
    public void EnviarNumeroCuentaPTC(String cuenta){    	
    	try {    		
    		WebDriverWait wait = new WebDriverWait(driver, 10);
    		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.bcp.bank.bcp:id/layDestiny")));
    		
    		driver.findElement(By.id(CapabilityBasePKG()+":id/layDestiny")).click();
    		driver.findElement(By.id(CapabilityBasePKG()+":id/etInput")).sendKeys(cuenta);    
    		
    		driver.hideKeyboard();
    		driver.navigate().back();
    	/*
    	driver.navigate().back();
    	try{
			if(driver.findElement(By.id("csSpinner")).isDisplayed())
			{
				return;
			}
		}
		catch(Exception ex)
		{
			driver.navigate().back();
		}*/
    	
    	
  
		} catch (Exception e) {
			// TODO: handle exception
			capturaPantalla(scenario);
			Assert.fail();
		}    	

    }
    
    
	public void EnviarNumeroPTOtrasFinan(String cuenta){
		
		try{
			driver.findElementByXPath("//android.widget.EditText[@resource-id='" + CapabilityBasePKG() + ":id/etInput' and contains(@text,'Introduce')]").click();
		}
		catch(Exception e){
			driver.scrollTo("Empresa");
	    	driver.findElementByXPath("//android.widget.EditText[@resource-id='" + CapabilityBasePKG() + ":id/etInput' and contains(@text,'Introduce')]").click();
		}
		driver.findElementByXPath("//android.widget.EditText[@resource-id='" + CapabilityBasePKG() + ":id/etInput']").sendKeys(cuenta);	
/*NICK	
		driver.navigate().back();
		driver.navigate().back();
*/
	}
    

    
	
	public void SeleccionardeLista(String texto){

		try{
			driver.findElementByXPath("//android.widget.RelativeLayout/android.widget.TextView[@text='" + texto + "']").click();
			Thread.sleep(4000);
		}catch(Exception e){
	    	driver.navigate().back();
	    	driver.navigate().back();
		}
		
	}
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**CONSULTAS Y MOVIMIENTOS**/
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public void SeleccionarSegunNumero(String texto){
		try {
			driver.findElementByXPath("//android.widget.TextView[@resource-id='"+ CapabilityBasePKG() +":id/tvNumber' and @text='" + texto +"']").click();			
		} catch (Exception e) {
			// TODO: handle exception
			driver.scrollTo(texto);
			driver.findElementByXPath("//android.widget.TextView[@resource-id='"+ CapabilityBasePKG() +":id/tvNumber' and @text='" + texto +"']").click();
		}		
	}
	
	
	public void VisualizarMovimientos(){
		
		int Cantidad;
		try {		
			MobileElement elemento=driver.findElementById(CapabilityBasePKG()+":id/listView");
	        
	        int posicionX = elemento.getLocation().getX();
	        int posicionY = elemento.getLocation().getY();
	        System.out.println("   X: " + posicionX + " Y: " + posicionY); 
	        Cantidad = driver.findElementsByXPath("//android.widget.RelativeLayout/android.widget.ListView/android.widget.FrameLayout").size();
			 
			for(int i=0;i<Cantidad;i=i+2){
				if (CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")){
					driver.swipe(posicionX, posicionY, posicionX, posicionY -450, 3000);
				}else if(CapabilityDeviceName().equals("MWS7N17118009373")){
					driver.swipe(posicionX, posicionY, posicionX, posicionY -900, 3000);
				}else{
					driver.swipe(posicionX, posicionY, posicionX, posicionY -900, 3000);
				}
			}			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**RECARGA DE CELULAR
	 * @throws InterruptedException **/
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void SeleccionaFormaPago(String seleccion) throws InterruptedException{
		String valorActual;
		MobileElement elemento;
		
		try{
			//valorActual = driver.findElement(By.xpath("//android.widget.RelativeLayout[@resource-id='" +CapabilityBasePKG()+ ":id/csSpinner']/android.widget.TextView")).getText();
			
			if(driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).getText().contains("Recarga de celular")){
				valorActual = driver.findElementByXPath("//android.widget.RelativeLayout[@index=0]/android.widget.LinearLayout[@index=0]/android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']/android.widget.TextView").getText();
				elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@index=0]/android.widget.LinearLayout[@index=0]/android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']");	
			}else{
				valorActual = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/csSpinner']/android.widget.TextView").getText();
				elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']");
			}			
			
			
			if(!valorActual.toUpperCase().equals(seleccion.toUpperCase())){
				//MobileElement elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='" +CapabilityBasePKG()+ ":id/csSpinner']");
			       
			    int posicionX = elemento.getLocation().getX();
			    int posicionY = elemento.getLocation().getY();
			    System.out.println("   X: "+ posicionX + ", Y: "+posicionY);
			       
			    TouchAction action = new TouchAction((MobileDriver)driver);
	    		elemento.click();
	    		Thread.sleep(1000);
			        
			    if (CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002") || CapabilityDeviceName().equals("ZY2236767P")){
			    	if(seleccion.toUpperCase().equals("TARJETAS")){
			            action.tap(posicionX +10,posicionY + 180).perform();
			    	}
			    }else if(CapabilityDeviceName().equals("MWS7N17118009373")){ //HUAWEI P9
			    	if(seleccion.toUpperCase().equals("TARJETAS")){
			            action.tap(posicionX +30,posicionY + 244).perform();
			    	}
		        }else{		        	
		        	if(seleccion.toUpperCase().equals("TARJETAS")){
		        		action.tap(posicionX +20,posicionY + 380).perform();
		        	}		        	   
		        }
			}
			else{
				return;
			}
		}catch(Exception e){
			//	
		}
	}
	
	
	public void SeleccionaEmpresa(String seleccion) throws InterruptedException{
		Thread.sleep(3000);
		String valorActual = "";
		MobileElement elemento;
		
		if(driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).getText().contains("Recarga de celular") || driver.findElement(By.id(CapabilityBasePKG()+":id/tvTitle")).getText().contains("Favorito")){
			driver.scrollTo("Continuar");
			valorActual = driver.findElementByXPath("//android.widget.RelativeLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']/android.widget.TextView").getText();
			elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@index=2]/android.widget.LinearLayout[@index=0]/android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']");	
		}else{
			valorActual = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/csSpinner']/android.widget.TextView").getText();
			elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='" + CapabilityBasePKG() + ":id/csSpinner']");
		}
		
		if(!valorActual.toUpperCase().equals(seleccion.toUpperCase())){	
	        int posicionX = elemento.getLocation().getX();
	        int posicionY = elemento.getLocation().getY();
	        
	        System.out.println("   posicionX: "+ posicionX + ", posicionY:"+posicionY);	        
	        TouchAction action = new TouchAction((MobileDriver)driver);
	       
	        elemento.click();
	        Thread.sleep(1000);
	        
	        if (CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("ZY2236767P") || CapabilityDeviceName().equals("0A3C260D0F00E002")){	        	
	        	 if(seleccion.toUpperCase().equals("TDP")){	        		 
	        		 //action.tap(posicionX +20,posicionY + 110).perform();
	        		 action.tap(posicionX +20,posicionY + 162).perform();
	             }
	        }else if(CapabilityDeviceName().equals("MWS7N17118009373")){ //HUAWEI P9
	        	if(seleccion.toUpperCase().equals("TDP")){	 
	        		action.tap(posicionX +50,posicionY + 244).perform();
	        	}
	        }	        
	        else{	        	
	            if(seleccion.toUpperCase().equals("TDP")){
	              Thread.sleep(1000);
	              action.tap(posicionX +30,posicionY + 350).perform();
	           }	           
	        }
		} 
	}

	
	public void EnviarCelular(String numero){
		
		driver.scrollTo("Continuar");
    	MobileElement elemento= driver.findElement(By.name("Introduce un número"));
//NV   	elemento.click();  	
    	elemento.sendKeys(numero);
    		
    	driver.navigate().back();
    	driver.navigate().back();
	
	
    }
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**PAGO DE SERVICIOS
	 * @throws InterruptedException **/
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void SeleccionRecibosaPagar(String recibos){		
		int contador;
		String path;
		String [] recibo;
		int numRecibo;		
		
		recibo = recibos.split(",");		
		driver.scrollTo("Cancelar");
		contador = driver.findElementsByXPath("//android.widget.LinearLayout[@index='1']/android.widget.RelativeLayout").size();
		
		for(int j=0;j<recibo.length;j++){
			numRecibo = Integer.parseInt(recibo[j]) -1;			
			for(int i=0;i<contador;i++){
				path ="//android.widget.RelativeLayout[@index='" + Integer.toString(i) + "']/android.widget.ImageView[@resource-id='"+CapabilityBasePKG()+":id/ivCheck']";
				if( numRecibo == i){
					if(i<=2){
						driver.scrollTo("Titular");
						driver.findElement(By.xpath(path)).click();
						driver.scrollTo("Cancelar");
						break;
					}
					driver.findElement(By.xpath(path)).click();
					driver.scrollTo("Cancelar");
					break;
				}				
			}
		}			
	}
	
	
	public void SeleccionRecibosaPagarPASE(String recibos){
		
		int contador;
		String path;
		String [] recibo;
		int numRecibo;
		
		
		recibo = recibos.split(",");
		
		driver.scrollTo("Cancelar");
		contador = driver.findElementsByXPath("//android.widget.LinearLayout[@index='1']/android.widget.RelativeLayout").size();
		
		for(int j=0;j<recibo.length;j++)
		{
			numRecibo = Integer.parseInt(recibo[j]);
			
			for(int i=1;i<=contador;i++)
				
			{
				path ="//android.widget.RelativeLayout[@index='" + Integer.toString(i) + "']/android.widget.ImageView[@resource-id='"+CapabilityBasePKG()+":id/ivCheck']";
				if( numRecibo == i)
				{
					if(i<=2)
					{
						driver.scrollTo("Titular");
						driver.findElement(By.xpath(path)).click();
						driver.scrollTo("Cancelar");
						break;
					}
					driver.findElement(By.xpath(path)).click();
					driver.scrollTo("Cancelar");
					break;
				}
				
			}
		}
			
	}

		
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**FAVORITOS
	 * @throws InterruptedException 
	 * **/
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void SeleccionTipoFavorito(String opcion) throws InterruptedException{
		
		MobileElement elemento= driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/layFavoriteType']/android.widget.LinearLayout/android.widget.RelativeLayout");
		int x=elemento.getLocation().getX();
		int y=elemento.getLocation().getY();
		TouchAction action = new TouchAction((MobileDriver)driver);
		
		if(CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")){
			switch (opcion.toUpperCase()) {
				case "TARJETA":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +10,y + 180).perform();   
		         	  Thread.sleep(2000);
		         	  
					break;
				case "CELULAR":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +10,y + 280).perform();   
		         	  Thread.sleep(2000);	         	  
					break;
				case "SERVICIO":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +10,y + 380).perform();   
		         	  Thread.sleep(2000);
		         	  
					break;
				default:
					break;
			}
		}else if(CapabilityDeviceName().equals("MWS7N17118009373")){
			switch (opcion.toUpperCase()) {
				case "TARJETA":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x+30,y +250).perform();   
		         	  Thread.sleep(2000);			         	  
					break;
				case "CELULAR":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +30,y + 350).perform();   
		         	  Thread.sleep(2000);			         	  
					break;
				case "SERVICIO":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +30,y + 500).perform();   
		         	  Thread.sleep(2000);			         	  
					break;
				default:
					break;
				}
		}else{ //LGH81554bed16e
			switch (opcion.toUpperCase()) {
				case "TARJETA":
					action.tap(x,y).perform();
					Thread.sleep(2000);
					action.tap(x +20,y + 380).perform();   
					Thread.sleep(2000);
					break;
				case "CELULAR":
					action.tap(x,y).perform();
					Thread.sleep(2000);
					action.tap(x +20,y + 480).perform();   
		         	Thread.sleep(2000);
					break;
				case "SERVICIO":
					action.tap(x,y).perform();
		         	Thread.sleep(2000);
		         	action.tap(x +20,y + 630).perform();   
		         	Thread.sleep(2000);		         	  
					break;	
				default:
					break;
			}
			
		}
		
	}

	
	public void SeleccionTipoCuentaFavoritos(String opcion) throws InterruptedException{
		
		MobileElement elemento= driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/layAccountType']/android.widget.LinearLayout/android.widget.RelativeLayout");
		int x=elemento.getLocation().getX();
		int y=elemento.getLocation().getY();
		TouchAction action = new TouchAction((MobileDriver)driver);
		
		if(CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")){			
			if(!opcion.toUpperCase().equals("BCP")){
				action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +10,y + 180).perform();   
	         	  Thread.sleep(2000);	      
			}
		}else if(CapabilityDeviceName().equals("MWS7N17118009373")){				
				if(!opcion.toUpperCase().equals("BCP")){
					action.tap(x,y).perform();
		         	Thread.sleep(2000);
		         	action.tap(x +30,y + 304).perform();   
		         	Thread.sleep(2000);	      
				}
		}else{
			if(!opcion.toUpperCase().equals("BCP")){
				action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +20,y + 380).perform();   
	         	  Thread.sleep(2000);			
			}			
		}		
	}
	
	
	public void SeleccionTipoTarjetaFavoritos(String opcion) throws InterruptedException{
		
		MobileElement elemento= driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/layCardType']/android.widget.LinearLayout/android.widget.RelativeLayout");
		int x=elemento.getLocation().getX();
		int y=elemento.getLocation().getY();
		TouchAction action = new TouchAction((MobileDriver)driver);
		
		if(CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")){
			switch (opcion.toUpperCase()) {
			case "OTRAS FINANCIERAS":
				action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +10,y + 180).perform();   
	         	  Thread.sleep(2000);
	         	  
				break;
			case "OTROS BANCOS":
				action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +10,y + 300).perform();   
	         	  Thread.sleep(2000);
	         	  
				break;

			default:
				break;
			}
		}else if(CapabilityDeviceName().equals("MWS7N17118009373")){
			switch (opcion.toUpperCase()) {
				case "OTRAS FINANCIERAS":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +30,y + 250).perform();   
		         	  Thread.sleep(2000);
		         	  
					break;
				case "OTROS BANCOS":
					action.tap(x,y).perform();
		         	  Thread.sleep(2000);
		         	  action.tap(x +30,y + 350).perform();   
		         	  Thread.sleep(2000);		         	  
					break;

				default:
					break;
				}	
		}else{
			switch (opcion.toUpperCase()) {
			case "OTRAS FINANCIERAS":
				action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +20,y + 380).perform();   
	         	  Thread.sleep(2000);
	         	  
				break;
			case "OTROS BANCOS":
				  action.tap(x,y).perform();
	         	  Thread.sleep(2000);
	         	  action.tap(x +20,y + 480).perform();   
	         	  Thread.sleep(2000);
				break;
				
			default:
				break;
			}
			
		}
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**NOTIFICACIONES
	 * @throws InterruptedException 
	 * **/
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public void ObtenerNotificacion(String posicion){
		
		int cantidad;
		cantidad=driver.findElementsByXPath("//android.widget.ListView[@resource-id='"+CapabilityBasePKG()+":id/lvMessages']/android.widget.FrameLayout").size();
		
		for(int i=1;i<=cantidad;i++){
			
			if(i==Integer.parseInt(posicion)){	
			driver.findElementByXPath("//android.widget.ListView[@resource-id='"+CapabilityBasePKG()+":id/lvMessages']/android.widget.FrameLayout[@index='"+ posicion +"']").click();
			break;
			}
		}
		
	}
	
	public void EjecucionAccion(String texto) throws Exception{
		
		clickOnButtonByXPath2("TextView",texto);
		
		if(texto.toUpperCase().equals("BORRAR")){
			
			clickOnButtonByXPath2("Button",texto);
			driver.navigate().back();
		}else{
			driver.navigate().back();
		}
		loading(100,CapabilityBasePKG()+":id/rlNoFavs");
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/**PAGO COMPARTIDO
	 * @throws InterruptedException **/
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public boolean IsDisplayedByName(String texto){
		return driver.findElement(By.name(texto)).isDisplayed();

	}

	public void ClickByName(String texto)
		{
		driver.findElement(By.name(texto)).click();
			}

		public void SendTextById(String id, String texto)
		{

			driver.findElement(By.id(CapabilityBasePKG()+id)).sendKeys(texto);

		}

		public void seleccionarCuentas()
		{

			driver.findElementById(CapabilityBasePKG()+":id/layPaymentMethodSource").click();
		}

		public void seleccionarCuentaOrigen()
		{

			driver.findElementByName("Cuenta de origen").click();
		}

		public void seleccionardeListaFormaPago(String texto){
			MobileElement elem;
			elem = driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='1']/android.widget.RelativeLayout[@index='1']/android.widget.RelativeLayout[@index='1']"));
			int x,y;
			
			x = elem.getLocation().getX();
			y = elem.getLocation().getY();
			for(int i=0;i<100;i++)
			{
				try{
					
					driver.findElement(By.name(texto)).click();
					//driver.findElementByXPath("//android.widget.RelativeLayout/android.widget.TextView[@text='" + texto + "']").click();
					Thread.sleep(4000);
				
					driver.navigate().back();
					//driver.navigate().back();
					break;
				}
				catch(Exception ex)
				{
					if(CapabilityDeviceName().equals("0A3C260D0F00E002"))
					{
					driver.swipe(x, y, x, y - 100, 3000);
					}
					else
					{
						driver.swipe(x, y, x, y - 200, 3000);
					}
				}
			}
		}

		public void SeleccionaFormaPagoCompartido(String seleccion) throws InterruptedException{

			if(driver.findElementsByXPath("//android.widget.RelativeLayout[@index='1']/android.widget.RelativeLayout[@index='0']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@resource-id='" +CapabilityBasePKG()+ ":id/tvTitle' and @text='Forma de pago']").size()!=0)
			{
				MobileElement elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='" +CapabilityBasePKG()+ ":id/csSpinner']/android.widget.TextView[@text='Cuentas']");
				int posicionX;
				int posicionY;

				posicionX = elemento.getLocation().getX();
				posicionY = elemento.getLocation().getY();
				System.out.println("TamaÃ±o"+ posicionX + "tamaÃ±o2:"+posicionY);

				TouchAction action = new TouchAction((MobileDriver)driver);

				if (CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("0A3C260D0F00E002")){

					if(seleccion.toUpperCase().equals("TARJETAS")){

						action.tap(posicionX,posicionY).perform();
						Thread.sleep(2000);
						action.tap(posicionX +10,posicionY + 200).perform();   
						Thread.sleep(2000);
					}

				}else{

					if(seleccion.toUpperCase().equals("TARJETAS")){

						action.tap(posicionX,posicionY).perform();
						Thread.sleep(2000);
						action.tap(posicionX +20,posicionY + 400).perform();   
						Thread.sleep(2000);
					}

				}

			}  

		}


		public void SeleccionaTipoMoneda(String seleccion) throws InterruptedException{
		
				MobileElement elemento = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='"+CapabilityBasePKG()+":id/csSpinner']");	
		       
		        int posicionX = elemento.getLocation().getX();
		        int posicionY = elemento.getLocation().getY();
		        System.out.println("   X: " + posicionX + ", Y: " + posicionY);		        
		
		        TouchAction action = new TouchAction((MobileDriver)driver);
		       
		        if (CapabilityDeviceName().equals("ZY2233KD87") || CapabilityDeviceName().equals("ZY2236767P") || CapabilityDeviceName().equals("0A3C260D0F00E002")){		        	
		        	 if(seleccion.toUpperCase().equals("DOLARES")){
		        		 action.tap(posicionX +20,posicionY + 200).perform();   
		             }
		        	 else{
		        		 action.tap(posicionX +20,posicionY + 80).perform();  
		        	 }
		        }else if(CapabilityDeviceName().equals("MWS7N17118009373")){	//HUAWEI P9	        	
		            if(seleccion.toUpperCase().equals("DOLARES")){  
		            	action.tap(posicionX +30,posicionY + 244).perform();   
		            }
		            else{
		            	action.tap(posicionX +30,posicionY + 120).perform();   
		            }
		        }else{		        	
		            if(seleccion.toUpperCase().equals("DOLARES")){  
		            	action.tap(posicionX +30,posicionY + 350).perform();   
		            }
		            else{
		            	action.tap(posicionX +30,posicionY + 150).perform();   
		            }
		        }
		}

		public void ingresaMontoCompartido(String monto){			
			
			WebDriverWait wait = new WebDriverWait(driver, 500);
			wait.until(ExpectedConditions.elementToBeClickable(By.id("etTotalAmount")));
			
			driver.findElementById("etTotalAmount").click();
			driver.findElementById("etTotalAmount").sendKeys(monto);
			driver.navigate().back();
			driver.navigate().back();
		}
		

		public void seleccionarInterruptor(String id, String texto) throws InterruptedException{
			if(texto.equals("SI")){
				driver.scrollTo("Todos pagan el mismo monto");
				driver.findElementById(id).click();
			}
			else{
				System.out.println("No pagan mismo monto");
			}
		}
		

		public void ingresarParticipante(String participante){
			try{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'destinatario')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'destinatario')]")).sendKeys(participante);
			}
			catch(Exception e){
				driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'Participantes')]")).click();
				driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'Participantes')]")).sendKeys(participante);
				//driver.findElement(By.name("Participantes")).click();
				//driver.findElement(By.name("Participantes")).sendKeys(participante);
			}
			driver.navigate().back();
			driver.navigate().back();
		}


		public void ingresaCodigoValidacion(String participante){			   	
			try{
				/*
				Thread.sleep(2000);	
				String codigovalidacion = "";	
				DBConnection conexion = new DBConnection();
				codigovalidacion = conexion.ObtenerCodigoPagoCompartido(participante);		
				driver.findElementById("etInput").sendKeys(codigovalidacion);
				*/
				driver.findElementById("etInput").sendKeys(participante);
			}
			catch (Exception ex){
				System.out.println("Se produjo un error en el codigo");
			}			 		
		}		


		public void Validacion(){
			String ElemTipoCambio;
			String ElemContravalor;
			
			String SignoMonedaTC;
			String SignoMonedaCV;
			
			try {			
			int CantElem=driver.findElementsByXPath("//android.widget.LinearLayout[@resource-id='"+CapabilityBasePKG()+":id/llvalorcambios']/android.widget.LinearLayout").size();
			
//			for(int i=0;i<CantElem;i++){
			
				ElemTipoCambio= driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='"+CapabilityBasePKG()+":id/llvalorcambios']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@resource-id='"+CapabilityBasePKG()+":id/TvTypeCambio']").getText();
				ElemContravalor= driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='"+CapabilityBasePKG()+":id/llvalorcambios']/android.widget.LinearLayout[@index='1']/android.widget.TextView[@resource-id='"+CapabilityBasePKG()+":id/TvequivalentValue']").getText();
//				SignoMonedaTC=ElemTipoCambio.substring(0, 2);
//				SignoMonedaCV=ElemContravalor.substring(0, 2);
				
	/*				if(SignoMonedaTC.equals(SignoMonedaCV)){
						setTipoCambio(ElemTipoCambio);
						setContravalor(ElemContravalor);
						System.out.println("Tipo de Cambio Pantalla 1:" + ElemTipoCambio);
						System.out.println("Elemento Contravalor Pantalla 1:" + ElemContravalor);
						break;
						}*/
//					}
				} catch (Exception e) {
					Assert.fail();
				}
				
			}
			
			
		
		
		public void Validacion2(){
			String ElemTipoCambioPant2;
			String ElemContravalorPant2;
			
			Boolean ValorTipoCambio=false;
			Boolean ValorContraV=false;
			
			ElemTipoCambioPant2= driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='"+CapabilityBasePKG()+":id/llvalorcambios']/android.widget.LinearLayout[@index='0']/android.widget.TextView[@resource-id='"+CapabilityBasePKG()+":id/TvTypeCambio']").getText();
			ElemContravalorPant2= driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='"+CapabilityBasePKG()+":id/llvalorcambios']/android.widget.LinearLayout[@index='1']/android.widget.TextView[@resource-id='"+CapabilityBasePKG()+":id/TvequivalentValue']").getText();
			try {
				if(ElemTipoCambioPant2.equals(getTipoCambio())){
					ValorTipoCambio=true;
					System.out.println("Tipo de Cambio Pantalla 2:" + ElemTipoCambioPant2);
					try {
						if(ElemContravalorPant2.equals(getContravalor())){
							ValorContraV=true;
							System.out.println("Elemento Contravalor Pantalla 2:" + ElemContravalorPant2);		
						}
					} catch (Exception e) {
						System.out.println("El Tipo de Cambio coinciden y el Monto del ContraValor no Coinciden");
						Assert.fail();
					}
				}
			} catch (Exception e) {
				System.out.println("El Tipo de Cambio no Coinciden");
				Assert.fail();
			}	
					
				
			}
			
			

}
